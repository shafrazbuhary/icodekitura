// swift-tools-version:4.0
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "iCodeKitura",
    products: [
        // Products define the executables and libraries produced by a package, and make them visible to other packages.
        .library(
            name: "iCodeKitura",
            targets: ["iCodeKitura"]),
    ],
    dependencies: [

        // .package(url: /* package url */, from: "1.0.0"),
        .package(url: "https://github.com/IBM-Swift/Kitura.git", "2.0.0"..<"3.0.0"),
        .package(url: "https://mbmshafraz@bitbucket.org/shafrazbuhary/icode.git", from: "0.1.0"),
    ],
    targets: [
        // Targets are the basic building blocks of a package. A target can define a module or a test suite.
        // Targets can depend on other targets in this package, and on products in packages which this package depends on.
        .target(
            name: "iCodeKitura",
            dependencies: ["Kitura", "iCode"]),
        .testTarget(
            name: "iCodeKituraTests",
            dependencies: ["iCodeKitura"]),
    ]
)
