import XCTest
@testable import iCodeKituraTests

XCTMain([
    testCase(iCodeKituraTests.allTests),
])
